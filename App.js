import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TouchableOpacity, TextInput } from 'react-native';
import { useState } from 'react';


const Separator = () => <View style={styles.separator} />;


export default function App() {
	const [counter, setCounter] = useState(0);
	const [times, setTimes] = useState([]);
	const [countPerValue, setCountPerValue] = useState('1');

	const resetCount = () => {
		setCounter(0);
		setTimes([]);
	};
	
	const count = () => {
		setCounter(counter + 1);
		setTimes([...times, new Date().getTime() / 1000]);
	};

	var counts_per_second = -1;
	if (counter > 1)
	{
		counts_per_second = (counter - 1) / (times[times.length - 1] - times[0]);
	}

	var value_units_s = counts_per_second / parseFloat(countPerValue);
	var value_units_h = value_units_s * 3600;


	return (
		<View style={styles.container}>
			<View style={styles.buttons}>
				<TouchableOpacity style={styles.bigButton} title="Reset" onPress={resetCount}><Text style={styles.buttonText}>Reset</Text></TouchableOpacity>
				<TouchableOpacity style={styles.bigButton} title="Start/Count" onPress={count}><Text style={styles.buttonText}>Start/Count</Text></TouchableOpacity>
			</View>
			<Separator />
			<View style={styles.row}>
				<Text style={styles.cell}>Counts</Text>
				<Text style={styles.cell}>Counts/s</Text>
			</View>
			<View style={styles.row}>
				<Text style={styles.cell}>{counter}</Text>
				<Text style={styles.cell}>{counts_per_second}</Text>
			</View>
			<Separator />
			<Text style={styles.bodyText}>Counts per value unit:</Text>
			<TextInput
				style={styles.input}
				value={countPerValue}
				onChangeText={setCountPerValue}
			/>
			<Separator />
			<Text style={styles.bodyText}>{value_units_s} value units per s</Text>
			<Text style={styles.bodyText}>{value_units_h} value units per h</Text>
		</View>
	);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#222',
    textColor: '#ddd',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 8,
  },

  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  bigButton: {
    backgroundColor: '#888',
    padding: 20,
    borderRadius: 10,
  },
  buttonText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },

  bodyText: {
    color: '#ddd',
    fontSize: 20
  },

  row: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    //flex: 1,
  },

  cell: {
    marginRight: 10,
    color: '#ddd',
    fontSize: 20,
    flex: 1,
  },

  separator: {
    marginVertical: 8
  },

  input: {
    width: 200,
    height: 40,
    borderWidth: 1,
    paddingHorizontal: 10,
    marginTop: 10,
    borderColor: '#fff',
    color: '#fff',
  },
});
